let drinkList = [

    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "/Bilder/Coke.jpeg"
    },

    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "/Bilder/Fanta.jpeg"
    },

    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "/Bilder/Pepsi.jpeg"
    },

    {
        "name": "Red Bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "/Bilder/Redbull.jpeg"
    }
];



// Function to create a salad item HTML element

function createdrinkItem(drink) {

    return `
    <div>
        <img class="drink_image" src="${drink.imageUrl}" alt="${drink.name}">
            <p>${drink.name}</p>
        <select>
            <option>50cl</option>
            <option>100cl</option>
            <option>150cl</option>
        </select>
            ${drink.prize}
        <button onclick="increaseTheNumber()"><img class="small-size-flex" src="/Bilder/shoppingcart_48.png" alt="warenkorb"></button>
    </div>`;

}

// Get the container where salad items will be added

const drinkFlexContainer = document.getElementById('drinksTable');

// Loop through the salad data and add each salad item to the container

for (const drink of drinkList) {
    const drinkItemHTML = createdrinkItem(drink);
    drinkFlexContainer.innerHTML += drinkItemHTML;
}