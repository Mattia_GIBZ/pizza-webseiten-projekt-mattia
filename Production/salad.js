let saladList = [

    {
        "name": "Green salad with tomatoe",
        "prize": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        "imageUrl": "/Bilder/salad-tomatoe.jpeg"
    },

    {
        "name": "Tomato salad with mozzarella",
        "prize": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella"
        ],
        "imageUrl": "/Bilder/tomatoe-mozarella.jpeg"
    },

    {
        "name": "Field salad with egg",
        "prize": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            "Egg"
        ],
        "imageUrl": "/Bilder/salad-egg.jpeg"
    },

    {
        "name": "Rocket with Parmesan",
        "prize": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            "Parmesan"
        ],
        "imageUrl": "/Bilder/rocket-parmesan.jpeg"
    }
];



// Function to create a salad item HTML element

function createsaladItem(salad) {

    return `
    <div>
        <img class="salad_image" src="${salad.imageUrl}" alt="${salad.name}">
            <p>${salad.name}</p>
        <small class="salad_description">${salad.ingredients.join(', ')}</small><br>
        <select>
            <option>Italian dressing</option>
            <option>French dressing</option>
        </select>
            ${salad.prize}
        <button onclick="increaseTheNumber()"><img class="small-size-flex" src="/Bilder/shoppingcart_48.png" alt="warenkorb"></button>
    </div>`;

}

// Get the container where salad items will be added

const saladFlexContainer = document.getElementById('saladTable');

// Loop through the salad data and add each salad item to the container

for (const salad of saladList) {
    const saladItemHTML = createsaladItem(salad);
    saladFlexContainer.innerHTML += saladItemHTML;
}