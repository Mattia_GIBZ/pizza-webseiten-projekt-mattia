var pizzaList =
    [
        {
            "name": "Piccante",
            "prize": "16$",
            "id": 1,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Spicy Salami",
                "Chilies",
                "Oregano"
            ],
            "imageUrl": "/Bilder/Pizza_Piccante.jpg"
        },
        {
            "name": "Giardino",
            "prize": "14$",
            "id": 2,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Artichokes",
                "Fresh Mushrooms"
            ],
            "imageUrl": "/Bilder/Pizza_Giardino.png"
        },
        {
            "name": "Prosciuotto e funghi",
            "prize": "15$",
            "id": 3,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Ham",
                "Fresh Mushrooms",
                "Oregano"
            ],
            "imageUrl": "/Bilder/Pizza_Prosciutto.jpeg"
        },
        {
            "name": "Quattro formaggi",
            "prize": "13$",
            "id": 4,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Parmesan",
                "Gorgonzola"
            ],
            "imageUrl": "/Bilder/Pizza_Quattro_Formaggi.jpeg"
        },
        {
            "name": "Quattro stagioni",
            "prize": "17$",
            "id": 5,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Ham",
                "Artichokes",
                "Fresh Mushrooms"
            ],
            "imageUrl": "/Bilder/Pizza_Quattro_Stagioni.jpeg"
        },
        {
            "name": "Stromboli",
            "prize": "12$",
            "id": 6,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Fresh Chilies",
                "Olives",
                "Oregano"
            ],
            "imageUrl": "/Bilder/Pizza_Stromboli.jpeg"
        },
        {
            "name": "Verde",
            "prize": "13$",
            "id": 7,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Broccoli",
                "Spinach",
                "Oregano"
            ],
            "imageUrl": "/Bilder/Pizza_Verde.jpeg"
        },
        {
            "name": "Rustica",
            "prize": "15$",
            "id": 8,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Ham",
                "Bacon",
                "Onions",
                "Garlic",
                "Oregano"
            ],
            "imageUrl": "/Bilder/Pizza_Rustica.jpeg"
        }
    ];


function createPizza(pizza) {

    return`
    <div id="Pizzas" class="flex-container">
    <div class="pizza_container">
      <img src="${pizza.imageUrl}" alt="${pizza.name}" class="pizza_image">
      <div class="pizza_price">
        <div class="pizza_names">${pizza.name}</div>
        <div>${pizza.prize}<button onclick="increaseTheNumber()" ><img class="small-size-flex" src="/Bilder/shoppingcart_48.png" alt="warenkorb"></button></div>
      </div>
      <p class="pizza_description">${pizza.ingredients.join(', ')}</p>
    </div>`;

}



// Get the container where pizza items will be added

const pizzaFlexContainer = document.getElementById('Pizzas');



// Loop through the pizza data and add each pizza item to the container

for (const pizza of pizzaList) {
    const pizzaItemHTML = createPizza(pizza);
    pizzaFlexContainer.innerHTML += pizzaItemHTML;
}


function addToCart() {
    localStorage.setItem("cartAmount", localStorage.getItem("cartAmount")*1+1)
    let currentValue = localStorage.setItem("cartAmount");
    document.querySelector("cartAmount").innerHTML = currentValue;
}
